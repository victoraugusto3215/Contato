//
//  detalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

class detalhesViewController: UIViewController {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var endereco: UILabel!
    
    public var contato: Contato?
    override func viewDidLoad() {
        
        title = contato?.nome
        
        nome.text = contato?.nome
        numero.text = contato?.telefone
        email.text = contato?.email
        endereco.text = contato?.endereco
       
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
